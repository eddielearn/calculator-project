const numbers = document.querySelectorAll('.number');
const buttons = document.querySelectorAll('.button');
const display = document.querySelector('.display');
const operators = document.querySelectorAll('.button.operator');
const equals = document.querySelector('#equalButton');
const AC = document.querySelector('#AC');
display.textContent = '0';

let firstNumber = '';
let secondNumber = '';
let operator = '';
let typed = '';
const regex = /^(?!-0?(\.0+)?$)-?(0|[1-9]\d*)?(\.\d*)?$/;


numbers.forEach(number => {
    number.addEventListener('click', updateDisplay);
});


function updateDisplay(e) {
    let value = e.target.textContent;
    
    console.log(value);
    if (display.textContent == '0') {
        display.textContent = '';
    }

    display.textContent += value;
    typed = display.textContent;
    
    if (!regex.test(typed)) {
        display.textContent = 'Invalid';
    }
    
}


operators.forEach(opButton => {
    opButton.addEventListener('click', () => {
        if (firstNumber === '') {
            firstNumber = parseFloat(display.textContent);
            operator = opButton.textContent;
            display.textContent = '0';
        } else {
            secondNumber = parseFloat(display.textContent);
            const result = operate(operator, firstNumber, secondNumber);
            display.textContent = result;
            firstNumber = result; // Store the result as the new firstNumber
            operator = opButton.textContent;
        }
    });
});

equals.addEventListener('click', () => {
    if (firstNumber !== '' && operator !== '' && display.value !== '') {
        secondNumber = parseFloat(display.textContent);
        const result = operate(operator, firstNumber, secondNumber);
        display.textContent = result;
        firstNumber = '';
        operator = '';
        secondNumber = '';
    }
});

function operate(operator, a, b) {
    switch (operator) {
        case '+':
            return add(a, b);
        case '-':
            return subtract(a, b);
        case '*':
            return multiply(a, b);
        case '÷':
            return divide(a, b);
        default:
            return "undef";
    }
}

function add(a, b) {
    return a + b;
}

function subtract(a, b) {
    return a - b;
}

function multiply(a, b) {
    return a * b;
}

function divide(a, b) {
    if (b === 0) {
        return "undefined";
    }
    return a / b;
}

AC.addEventListener('click', () => {
    display.textContent = '0';
    firstNumber = '';
    operator = '';
    secondNumber = '';
});